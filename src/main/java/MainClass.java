public class MainClass {

    private final Integer class_number = 20;
    private final String class_string = "Hello, world";

    public Integer getLocalNumber() {
        return 14;
    }

    public Integer getClassNumber() {
        return class_number;
    }

    public String getClassString() {
        return class_string;
    }
}
