import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainClassTest {
    MainClass mainClass = new MainClass();

    @Test
    public void testGetLocalNumber() {
        assertEquals(14, mainClass.getLocalNumber(), "Число localNumber должно ровняться 14");
    }

    @Test
    public void testGetClassNumber() {
        assertTrue(mainClass.getClassNumber() > 45, "Число classNumber должно быть больше 45");
    }

    @Test
    public void testGetClassString() {
        assertTrue(mainClass.getClassString().contains("hello") || mainClass.getClassString().contains("Hello"),
                "Строка ClassString не содержит подстроку Hello или hello");
    }
}